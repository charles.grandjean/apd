#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include "gtest/gtest.h"

class Nombre{
public:
    Nombre(){premier_ = nullptr;};    
    Nombre & operator=(const Nombre & nbr){return * new Nombre(nbr);};
    Nombre(const Nombre & n){premier_ = new Chiffre(*n.premier_);};
    Nombre(unsigned int n){premier_ = new Chiffre(n);};

    friend std::ostream &operator<<(std::ostream &out, const Nombre &n){
      if (n.premier_ != nullptr){
        n.premier_->disp(out);
      }
      return out;
    };

private:
    struct Chiffre{
      unsigned int chiffre_;// entre 0 et 9
      Chiffre * suivant_; // chiffre de poids supérieur ou nullptr
    
      Chiffre(const Chiffre & n){
        chiffre_ = n.chiffre_;
        if (n.suivant_){
          suivant_ = new Chiffre(*n.suivant_);
        } 
        else{
          suivant_ = nullptr;
        }
      }


      Chiffre(unsigned int n){
        chiffre_ = n%10;
        if (n>10){
          suivant_ = new Chiffre(n/10);
        }
      }
      void disp(std::ostream &out){
        if (suivant_){suivant_->disp(out);}
        out << chiffre_;
      }
    };
    Chiffre * premier_;
};

TEST(TestNombre, TestNombreVide)
{
    Nombre n;
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "");
}

TEST(TestNombre, TestNombreNul)
{
    Nombre n{0};
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "0");
}

TEST(TestNombre, TestNombre12345678)
{
    Nombre n{12345678};
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "12345678");
}

TEST(TestNombre, TestNombreAffectation)
{
    Nombre n2;
    Nombre n1{12345678};
    n2 = n1;
    std::ostringstream os;
    os << n2;
    EXPECT_EQ(os.str(), "12345678");
}

TEST(TestNombre, TestNombreCopie)
{
    Nombre *n1 = new Nombre(12345678);
    Nombre *n2 = new Nombre(*n1);
    std::ostringstream os1;
    os1 << n1;
    std::ostringstream os2;
    os2 << n2;
    EXPECT_EQ(os1.str(), "12345678");
    EXPECT_EQ(os2.str(), "12345678");
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
