#include "Expression.h"

int Expression::NB_OBJECTS = 0;
Expression::Expression() {
  NB_OBJECTS++;
}
Expression::~Expression() {
  NB_OBJECTS--;
}

int Expression::nb_instances() {
  return NB_OBJECTS;
}
std::ostream& operator <<(std::ostream &out, const Expression &res) {
	return res.affiche(out);
}
