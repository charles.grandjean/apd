#include "Nombre.h"

Nombre::Nombre(int num)
      : val(num) {}
Nombre::Nombre()
      : Nombre(0) {}
Nombre* Nombre::derive(const std::string &derivable) const {
	return new Nombre(0);
}
std::ostream& Nombre::disp(std::ostream &out) const {	return out << val;}

Nombre* Nombre::clone() const {
	return new Nombre(valeur_);
}
Nombre::Nombre(double val) : Expression::Expression()
{
	this->value = val;
}

Nombre::Nombre(const Nombre & n) : Expression::Expression(n){
	this->value = n.value;
}
