#include <string>
#include "Expression.h"

class Variable: public Expression {
public:
	Variable(const std::string & var);
	virtual std::ostream& disp(std::ostream &out) const override;
	virtual Expression* derive(const std::string &derivable) const override;
	virtual Expression* clone() const override;
private:
	const std::string var;
};
