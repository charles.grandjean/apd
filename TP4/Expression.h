#include <iostream>
#include <string>

class Expression {
private:
  static int NB_OBJECTS;
public:
  Expression();
	virtual ~Expression();
	virtual std::ostream & affiche(std::ostream & out) const = 0;
	virtual Expression * derive(const std::string & var) const = 0;
  	static int nb_instances();
	virtual Expression * clone() const = 0;
};

std::ostream & operator << (std::ostream & out, const Expression & res);
