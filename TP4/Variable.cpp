#include "Variable.h"
#include "Nombre.h"

Variable::Variable(const std::string &name_var)
        : var(name_var){
}
std::ostream& Variable::disp(std::ostream &out) const {
	return out << var;
}

Expression* Variable::clone() const {
	return new Variable(var);
}
Expression* Variable::derive(const std::string &derivable) const {
	if (var != derivable) {
		return new Nombre(0);
	} else {
		return new Nombre(1);
	}
}
