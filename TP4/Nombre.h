#include "Expression.h"

class Nombre : public Expression {
public:
	Nombre();
	Nombre(int num);
	virtual std::ostream& disp(std::ostream &out) const =0;
	virtual Nombre* derive(const std::string & derivable) const override;
	virtual Nombre* clone() const override;
private:
	const int val;
};
