#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>

bool running = true;

void stop_handler(int sig) {
  printf("\nsignal %d\n", sig);
  printf("Program stopped\n");
  running = false;
}

int main(int argc, char * argv[]) {
    
  struct sigaction action;

  action.sa_handler = stop_handler; //link the function to the action
  action.sa_flags = 0;

  sigaction (SIGINT, &action, NULL);
  sigaction(SIGTERM,&action,NULL);

  printf("hello\n");

  while(running) {
    // PID :
    int currentid = getpid();
    int fatherid = getppid();

    printf("current id : %d\n", currentid);
    printf("father id : %d\n", fatherid);

    // Random value
    int r;
    r=rand() % 26;
    printf("random %d\n", r);

    sleep(1);
  }
  printf("bye\n");
  return EXIT_SUCCESS;
}